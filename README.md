
Pragmatic debian packaging for KeepassXC-Mail for Thunderbird
=============================================================


See also
--------

https://github.com/thsmi/sieve


Building
--------

The build will need *nodejs* >= 18, which can be obtained following the [official guide](https://github.com/nodesource/distributions/blob/master/README.md#using-ubuntu-1).

Then the build is canonical:

    $ dpkg-buildpackage


Obtaining a new upstream version
--------------------------------

    $ git remote add upstream https://github.com/thsmi/sieve.git
    $ git fetch upstream
    $ git checkout debian
    $ git merge … # version tag
